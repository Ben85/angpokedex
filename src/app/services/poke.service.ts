import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokeService {

 private BASE_URL = 'https://pokeapi.co/api/v2';


  constructor(private http:HttpClient) { }

  getAllPokemon() {
    return this.http.get(`${this.BASE_URL}/pokemon`);
  }

  getNextPage(nextUrl: string) {
    return this.http.get(nextUrl);
  }

}
