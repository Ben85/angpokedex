import { Component, OnInit, Input } from '@angular/core';
import { PokeService } from 'src/app/services/poke.service';

@Component({
  selector: 'app-poke-card',
  templateUrl: './poke-card.component.html',
  styleUrls: ['./poke-card.component.css']
})
export class PokeCardComponent implements OnInit {
  @Input() name;
  @Input() detailUrl;
  pokeImg: string;

  constructor(private pokeService: PokeService) { }

  ngOnInit() {
    this.pokeService.getNextPage(this.detailUrl).subscribe( (res:any)=>{
      this.pokeImg = res.sprites.front_default;
      console.log(this.pokeImg);
    }, (err)=>{
      console.log(err);
    });
  }

}
