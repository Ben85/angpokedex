import { Component, OnInit } from '@angular/core';
import { PokeService } from 'src/app/services/poke.service';

@Component({
  selector: 'app-poke-list',
  templateUrl: './poke-list.component.html',
  styleUrls: ['./poke-list.component.css']
})
export class PokeListComponent implements OnInit {
  pokeList: any;
  next: string;
  previous: string;

  constructor(private pokeService: PokeService) { }

  ngOnInit() {
    this.pokeService.getAllPokemon().subscribe( (res:any) => {
      this.pokeList = res.results;
      this.next = res.next;
      this.previous = res.previous;
      console.log(res);
    }, err => {
      console.log(err);
    });
  }

  updatePage(url:string) {
    this.pokeService.getNextPage(url).subscribe( (res:any)=> {
      this.pokeList = res.results;
      this.next = res.next;
      this.previous = res.previous;
    }, (err) => {
      console.log(err);
    });
  }

}
